/*
 *
 * The DbUnit Database Testing Framework
 * Copyright (C)2002-2004, DbUnit.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package org.dbunit.operation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultTableMetaData;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.util.QualifiedTableName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Manuel Laflamme
 * @since Jan 17, 2004
 * @version $Revision$
 */
public abstract class AbstractOperation extends DatabaseOperation
{

    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(AbstractOperation.class);

    protected String getQualifiedName(final String prefix, final String name, final IDatabaseConnection connection)
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("getQualifiedName(prefix={}, name={}, connection={}) - start",
                    new Object[] {prefix, name, connection});
        }

        final String escapePattern = (String)connection.getConfig().getProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN);
        final QualifiedTableName qualifiedTbleName = new QualifiedTableName(name, prefix, escapePattern);
        return qualifiedTbleName.getQualifiedName();
    }

    /**
     * Returns the metadata to use in this operation. It is retrieved
     * from the database connection using the information from the physical
     * database table.
     *
     * @param connection the database connection
     * @param metaData the XML table metadata
     */
    static ITableMetaData getOperationMetaData(final IDatabaseConnection connection,
            final ITableMetaData metaData) throws DatabaseUnitException, SQLException
    {
        logger.debug("getOperationMetaData(connection={}, metaData={}) - start", connection, metaData);

        final IDataSet databaseDataSet = connection.createDataSet();
        final String tableName = metaData.getTableName();

        final ITableMetaData tableMetaData = databaseDataSet.getTableMetaData(tableName);
        final Column[] columns = metaData.getColumns();

        final List<Column> columnList = new ArrayList<>();
        for (int j = 0; j < columns.length; j++)
        {
            final String columnName = columns[j].getColumnName();
            // Check if column exists in database
            // method "getColumnIndex()" throws NoSuchColumnsException when columns have not been found
            final int dbColIndex = tableMetaData.getColumnIndex(columnName);
            // If we get here the column exists in the database
            final Column dbColumn = tableMetaData.getColumns()[dbColIndex];
            columnList.add(dbColumn);
        }

        return new DefaultTableMetaData(tableMetaData.getTableName(),
                columnList.toArray(new Column[0]),
                tableMetaData.getPrimaryKeys());
    }
}
