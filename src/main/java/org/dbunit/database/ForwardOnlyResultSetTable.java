/*
 *
 * The DbUnit Database Testing Framework
 * Copyright (C)2002-2004, DbUnit.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package org.dbunit.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.RowOutOfBoundsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Manuel Laflamme
 * @since Apr 10, 2003
 * @version $Revision$
 */
public class ForwardOnlyResultSetTable extends AbstractResultSetTable
{

    /**
     * Logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(ForwardOnlyResultSetTable.class);

    private int _lastRow = -1;
    private boolean _eot = false; // End of table flag

    public ForwardOnlyResultSetTable(final ITableMetaData metaData,
            final ResultSet resultSet) throws SQLException, DataSetException
    {
        super(metaData, resultSet);
    }

    public ForwardOnlyResultSetTable(final ITableMetaData metaData,
            final IDatabaseConnection connection) throws DataSetException, SQLException
    {
        super(metaData, connection);
    }

    public ForwardOnlyResultSetTable(final String tableName, final String selectStatement,
            final IDatabaseConnection connection) throws DataSetException, SQLException
    {
        super(tableName, selectStatement, connection);
    }

    // //////////////////////////////////////////////////////////////////////////
    // ITable interface


    @Override
    public int getRowCount()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getValue(final int row, final String columnName) throws DataSetException
    {
        if(logger.isDebugEnabled())
            logger.debug("getValue(row={}, columnName={}) - start", Integer.toString(row), columnName);

        try
        {
            // Move cursor forward up to specified row
            while (!_eot && row > _lastRow)
            {
                _eot = !_resultSet.next();
                _lastRow++;
            }

            if (row < _lastRow)
            {
                throw new UnsupportedOperationException("Cannot go backward!");
            }

            if (_eot || row > _lastRow)
            {
                // Proactively close the resultset
                close();
                throw new RowOutOfBoundsException(row + " > " + _lastRow);
            }

            final int columnIndex = getColumnIndex(columnName);
            final Column column = _metaData.getColumns()[columnIndex];
            return column.getDataType().getSqlValue(columnIndex + 1, _resultSet);
        }
        catch (final SQLException e)
        {
            throw new DataSetException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder(2000);

        sb.append(super.toString());
        sb.append(", ");
        sb.append(getClass().getName()).append("[");
        sb.append("_eot=[").append(_eot).append("], ");
        sb.append("_lastRow=[").append(_lastRow).append("]");
        sb.append("]");

        return sb.toString();
    }
}
