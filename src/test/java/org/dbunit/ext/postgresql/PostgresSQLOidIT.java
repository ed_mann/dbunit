package org.dbunit.ext.postgresql;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.StringReader;
import java.sql.Statement;
import java.sql.Types;

import org.dbunit.DatabaseEnvironment;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.xml.sax.InputSource;

class PostgresSQLOidIT {
    private IDatabaseConnection _connection;

    // @formatter:off
    private static final String xmlData = "<?xml version=\"1.0\"?>" +
            "<dataset>" +
            "<T2 DATA=\"[NULL]\" />" +
            "<T2 DATA=\"\\[text UTF-8](Anything)\" />" +
            "</dataset>";
    // @formatter:on

    @BeforeEach
    protected void setUp() throws Exception {
        // Load active postgreSQL profile and connection from Maven pom.xml.
        _connection = DatabaseEnvironment.getInstance().getConnection();
    }

    @AfterEach
    protected void tearDown() throws Exception {
        if (_connection != null) {
            _connection.close();
            _connection = null;
        }
    }

    @Test
    void xtestOidDataType() throws Exception {
        final String testTable = "t2";
        assertThat(_connection).as("didn't get a connection").isNotNull();
        final DatabaseConfig config = _connection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new PostgresqlDataTypeFactory());
        final Statement stat = _connection.getConnection().createStatement();
        // DELETE SQL OID tables
        stat.execute("DROP TABLE IF EXISTS " + testTable + ";");

        // Create SQL OID tables
        stat.execute("CREATE TABLE " + testTable + "(DATA OID);");
        stat.close();

        try {
            final ReplacementDataSet dataSet = new ReplacementDataSet(new FlatXmlDataSetBuilder()
                    .build(new InputSource(new StringReader(xmlData))));
            dataSet.addReplacementObject("[NULL]", null);
            dataSet.setStrictReplacement(true);

            IDataSet ids;
            ids = _connection.createDataSet();
            final ITableMetaData itmd = ids.getTableMetaData(testTable);
            final Column[]       cols = itmd.getColumns();
            ids = _connection.createDataSet();
            for (final Column col : cols) {
                assertThat(col.getDataType().getSqlType()).isEqualTo(Types.BIGINT);
                assertThat(col.getSqlTypeName()).isEqualTo("oid");
            }

            DatabaseOperation.CLEAN_INSERT.execute(_connection, dataSet);
            ids = _connection.createDataSet();
            final ITable it = ids.getTable(testTable);
            assertNull(it.getValue(0, "DATA"));
            assertThat("\\[text UTF-8](Anything)".getBytes()).isEqualTo(it.getValue(1, "DATA"));
        } catch (final Exception e) {
            assertEquals("DatabaseOperation.CLEAN_INSERT... no exception",
                    "" + e);
        }
    }
}
